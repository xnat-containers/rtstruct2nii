#!/bin/bash

docker save rtstruct2composite_mask:v2 >rtstruct2composite_mask_v2.tar

scp rtstruct2composite_mask_v2.tar gtc2020-dev1:/tmp
ssh gtc2020-dev1 chmod a+r /tmp/rtstruct2composite_mask.tar
ssh gtc2020-dev1 docker load -i /tmp/rtstruct2composite_mask.tar

