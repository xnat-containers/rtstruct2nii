import nibabel as nib, numpy as np, argparse, sys

def split_masks(entries, outfile):
    if len(entries) >= 1:
        try:
            img = nib.load( entries[0].fileName())
            cmp = np.where( img.get_fdata()>0,0,0)

            for entry in entries:
                img = nib.load( entry.fileName())
                if img.shape != cmp.shape:
                    print('Dimensions of input images don\'t match.')
                    return False
                msk = np.where( img.get_fdata() > 0,1,0) * entry.value
                cmp = np.multiply( msk, np.where(cmp==0,1,0)) + cmp

        except Exception as e:
            print('ERROR: cannot read input file(s)')
            print( type(e))
            print( e.args)
            print( e)
            return False

        out = nib.Nifti1Image(cmp, img.affine, img.header)

    try:
        nib.save(out,outfile)
    except:
        print('ERROR: cannot write output file')
        return False
    return True

class MaskEntry:
    def __init__(self, rootPath, label, value):
        self.rootPath = rootPath
        self.label = label
        self.value = int(value)

    def fileName(self):
        # return self.rootPath + "/mask_" + self.label + ".nii.gz"
        return self.label


class DefParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

def main( args):
    #     p=DefParser(description='Convert n masks into a composite-mask')
    #     p.add_argument('mask1',type=str,help='first mask')
    #     p.add_argument('mask1_target',type=str,help='target value for mask1')
    #     p.add_argument('mask2',type=str,help='second mask')
    #     p.add_argument('mask2_target',type=str,help='target value for mask2')
    #     p.add_argument('outfile',type=str,help='output mask')
    #     a=p.parse_args()
    print ('split_masks ', *args)
    if (len(args) % 2) == 0:
        rootPath = args[0]
        outfile = args[1]
        entries = []
        for i in range(2, len(args)-1, 2):
            print(i)
            entry = MaskEntry( rootPath, args[i], args[i+1])
            entries.append(entry)

        sys.exit(split_masks( entries, outfile))
    else:
        print('Invalid number of arguments.')



if __name__=="__main__":
    main(sys.argv[1:])

