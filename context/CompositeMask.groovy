#!/usr/bin/env groovy

package org.nrg.xnat.ml

MaskCompositor compositor = new MaskCompositor()
compositor.run(args)


class MaskCompositor {

//    public static void main(String[] args) {
//        CompositeMaskLocal_script compositeMaskLocal = new CompositeMaskLocal_script()
//        compositeMaskLocal.run(args)
//    }

    public void run(String[] args) {

        Arguments arguments = new Arguments(args);

        if (!arguments.isValid()) {
            System.out.println("Arguments are not valid.")
            System.out.println(arguments)
            System.exit(1)
        }

//        String executable = "/Users/drm/Library/Python/3.7/bin/dcmrtstruct2nii"
        String executable = "dcmrtstruct2nii"
//        ShellProcess cmd = new ShellProcess( "${executable}", "convert", "-r", "${dataRoot}/AIM_20200304_162252.dcm", "-d", "${dataRoot}/scans/5_noncontrast/DICOM", "-o", "${dataRoot}", "-s", "kidney,spine", "-vvv")
//        ShellProcess cmd = new ShellProcess( "${executable}", "convert", "-r", "input/rtstruct/${rtstructFileName}", "-d", "input/dcm", "-o", "/output", "-g", "false", "-n", "-c", "false", "-vv")
        ShellProcessor cmd = new ShellProcessor("${executable}", "convert", "-r", "${arguments.rtstructFileName}", "-d", "${arguments.scanDirName}", "-o", "${arguments.outputDirName}", "-g", "false", "-n", "-c", "false", "-vv")
        System.out.println("${cmd.command}")
        int status = cmd.run();

        System.out.println("Status ${status}")
        System.out.println("StdOut: ${cmd.stdOut}")
        System.out.println("StdErr: ${cmd.stdErr}")

        String[] combine_cmd = buildCompositeCommand(arguments)
        ShellProcessor combine_proc = new ShellProcessor(combine_cmd)

        System.out.println("${combine_proc.command}")
        status = combine_proc.run();

        System.out.println("Status ${status}")
        System.out.println("StdOut: ${combine_proc.stdOut}")
        System.out.println("StdErr: ${combine_proc.stdErr}")

        // delete all the individual-region mask files.
        File outputDir = new File( "${arguments.outputDirName}");
        outputDir.eachFile {f -> if( ! f.name.matches( "mask_combined.nii")) f.delete()}
    }

    String[] buildCompositeCommand(Arguments arguments) {
        List<String> cmdList = new ArrayList<>()
        String combine_executable = "python3"
//        String script = "/Users/drm/projects/nrg/gtc2020/compositor/src/main/python/mask_convert2.py"
        String script = "mask_convert2.py"
        String rootDir = arguments.outputDirName
        String outFileName = String.format("%s/%s", rootDir, "mask_combined.nii")

        cmdList.add(combine_executable)
        cmdList.add(script)
        cmdList.add(rootDir)
        cmdList.add(outFileName)
        for (RoiEntry roiEntry : arguments.roiEntries) {
            // dcmrtstruct2nii replaces spaces in region labels with hyphens.
            cmdList.add(String.format("%s/%s", rootDir, roiEntry.maskFileName.replace(" ", "-")))
            cmdList.add(String.format("%d", roiEntry.maskValue))
        }
        return cmdList.toArray(new String[cmdList.size()])
    }

    class RoiEntry {

        String maskLabel
        int maskValue

        public RoiEntry(String maskLabel, String maskValue) {
            this.maskLabel = maskLabel
            this.maskValue = maskValue.toInteger()
        }

        String getMaskFileName() {
            return String.format("mask_%s.nii", maskLabel)
        }

        String toString() {
            StringBuilder sb = new StringBuilder()
            sb.append("RoiEntry[")
            sb.append("[maskLabel=${maskLabel}]")
            sb.append("[maskValue=${maskValue}]")
            sb.append("]")
            return sb.toString()
        }
    }

    class Arguments {
        String rtstructFileName;
        String scanDirName;
        String outputDirName;
        List<RoiEntry> roiEntries;

        public Arguments(String[] args) {
            roiEntries = new ArrayList<>()
            int i = 0
            while (i < args.size()) {
                switch (args[i]) {
                    case "-s":
                    case "-scan":
                        i++;
                        scanDirName = (i < args.size()) ? args[i] : null
                        i++
                        break;
                    case "-r":
                    case "-rtstruct":
                        i++;
                        rtstructFileName = (i < args.size()) ? args[i] : null
                        i++
                        break;
                    case "-o":
                    case "-output":
                        i++;
                        outputDirName = (i < args.size()) ? args[i] : null
                        i++
                        break;
                    case "-m":
                    case "-mask":
                        i++
                        String maskFileString = (i < args.size()) ? args[i] : null
                        i++
                        roiEntries = parse( maskFileString)
                        break;
                    default:
                        System.err.println("Unknown argument: ${args[i]}")
                        i++
                }
            }
        }

        public boolean isValid() {
            return rtstructFileName != null && scanDirName != null && outputDirName != null && (!roiEntries.isEmpty())
        }

        List<RoiEntry> parse( String maskFileString) {
            List<RoiEntry> entries =  new ArrayList<RoiEntry>();
            new File( maskFileString).eachLine { line ->
                String[] maskProperties = line.split(",")
                for( String maskProperty: maskProperties) {
                    String[] tokens = maskProperty.split("=")
                    entries.add( new RoiEntry( tokens[0].trim(), tokens[1].trim()))
                }
            }
            return entries;
        }

        @Override
        String toString() {
            StringBuilder sb = new StringBuilder()
            sb.append("Attributes[")
            sb.append("[rtstructFileName=${rtstructFileName}]")
            sb.append("[scanDirName=${scanDirName}]")
            sb.append("[outputDirName=${outputDirName}]")
            for (RoiEntry entry : roiEntries) {
                sb.append(entry)
            }
            sb.append("]")

            return sb.toString()
        }
    }
}

class ShellProcessor {

    private String[] cmd;
    private String stdOutString;
    private String stdErrString;
    private ProcessBuilder builder;

    public ShellProcessor( String ... cmd) {
        this.cmd = cmd
        builder = new ProcessBuilder( cmd).directory( new File(System.getProperty("user.dir")))
    }

    public int run() {
        println( "${builder.directory()}")
        Process process = builder.start()

        ByteArrayOutputStream stdOutArray = new ByteArrayOutputStream();
        ByteArrayOutputStream stdErrArray = new ByteArrayOutputStream();

        process.consumeProcessOutput( stdOutArray, stdErrArray)
        process.waitFor()

        stdOutString = stdOutArray.toString()
        stdErrString = stdErrArray.toString()

        return process.exitValue();
    }

    String[] getCommand() {
        return cmd;
    }

    String getStdOut() {
        return stdOutString
    }

    String getStdErr() {
        return stdErrString
    }
}
